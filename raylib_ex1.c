#include "raylib.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1920;
    const int screenHeight = 1080;

    int posx=screenWidth/2;
    int posy=0;
    int endposx=screenWidth/2;
    int endposy=screenHeight;
    
    int x=0;
    int y=screenHeight/2;
    int endx=screenWidth;
    int endy=screenHeight/2;
    
    
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        if (IsKeyDown(KEY_R)){
            posx+= 2.0f;
        }
        if (IsKeyDown(KEY_F)){   
            endposx+= 2.0f;
        }
        if (IsKeyDown(KEY_E)){
            posx-= 2.0f;
        }
         if (IsKeyDown(KEY_D)){
             endposx-= 2.0f;
         }
        if (IsKeyDown(KEY_Q)){
            y-= 2.0f;
        }
         if (IsKeyDown(KEY_W)){
            endy-= 2.0f;
        }
        if (IsKeyDown(KEY_A)){
            y+= 2.0f;
        }  
        if (IsKeyDown(KEY_S)){
            endy+= 2.0f;
        }   
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(SKYBLUE);
            DrawText("Marck\n", 850, 450, 40, RED);
            DrawLine(posx, posy, endposx, endposy,(Color){ 03, 21, 23, 255 } );
            DrawLine(x, y, endx, endy,(Color){ 03, 21, 23, 255 } );

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}