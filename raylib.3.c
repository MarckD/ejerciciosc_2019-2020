#include "raylib.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1280;
    const int screenHeight = 720;
    const int velocidady = 4;
    const int BallSize = 25;
    
    int posx=screenWidth-100;
    int posy=screenHeight/2-100;
    int x=50;
    int y=screenHeight/2-100;
    
    Vector2 ball;
    ball.x =screenWidth/2;
    ball.y =screenHeight/2;
    
    Vector2 ballVelocity;
    ballVelocity.x = 8;
    ballVelocity.y = 8;
    
    
    
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60); 
    
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        if (IsKeyDown(KEY_A)){
            y += velocidady;
        }           
        if (IsKeyDown(KEY_Q)){
          y -= velocidady;
        }
         if (IsKeyDown(KEY_DOWN)){
          posy += velocidady;
        }
         if (IsKeyDown(KEY_UP)){
          posy -= velocidady;
        }if (y<0){
            y=0;
        }
        if(y>screenHeight-200){
            y=screenHeight-200;
        }
        if (posy<0){
            posy=0;
        }
        if(posy>screenHeight-200){
            posy=screenHeight-200;
        }
        if (ball.x<BallSize){
            ballVelocity.x=-ballVelocity.x; //+2;
           // if (ballVelocity.x >=20 || ballVelocity.x >=-20){
           //     ballVelocity.x ==20;
           // }            
        }
        if (ball.x>screenWidth-BallSize){
            ballVelocity.x=-ballVelocity.x; //-2;
            //if (ballVelocity.x >=20 || ballVelocity.x >=-20){
            //    ballVelocity.x ==20;
            //}
        }
        if (ball.y<BallSize){
            ballVelocity.y=-ballVelocity.y; //+ 2;
            //if (ballVelocity.y >20 || ballVelocity.y >=-20){
            //    ballVelocity.y==20;
            //}
        }
        if (ball.y>screenHeight-BallSize){
            ballVelocity.y=-ballVelocity.y; //-2;
            //if (ballVelocity.y >=20 || ballVelocity.y >=-20){
            //    ballVelocity.y==20;
            //}
        }
              
           ball.x += ballVelocity.x;
           ball.y += ballVelocity.y;
           
       // rectngle necessito bool CheckCollisionCircleRec(Vector2 ball, float BallSize, Rectangle rec); 
         
     BeginDrawing();
     
     ClearBackground(SKYBLUE);
     DrawRectangle(posx,posy, 50, 200,BLUE); 
     DrawRectangle(x, y, 50, 200,BLUE); 
     DrawCircleV(ball, BallSize, YELLOW);   
     
     EndDrawing();
    }
    
    CloseWindow();  
      
    return(0);
}