/*******************************************************************************************
*
*   raylib example - Basic Game Structure
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013 Ramon Santamaria (Ray San - raysan@raysanweb.com)
*
********************************************************************************************/
 
#include "raylib.h"
 
typedef enum GameScreen {EXIT, GAMEPLAY, END } GameScreen;
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "Final PONG";
   
    GameScreen screen = GAMEPLAY;
   
    int timeCounter = 10;
    
    //personatge
    const int spriteWidth = 32;
    const int spriteHeight = 32;
   
    Color logoColor = BROWN;
    logoColor.a = 0;
 
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    //personatge
    Texture2D characters = LoadTexture("resource/characters.png");
    
    Rectangle sourceRec;
    sourceRec.x = 0;
    sourceRec.y = 32;//
    sourceRec.width = spriteWidth;  // 32
    sourceRec.height = spriteHeight; //32
   
    Rectangle destinationRec;
    destinationRec.x = screenWidth/2 - spriteWidth/2;
    destinationRec.y = screenHeight/2 - spriteHeight/2;
    destinationRec.width = sourceRec.width*4;
    destinationRec.height = sourceRec.height*4;
   
    Vector2 origin;
    origin.x = 0;
    origin.y = 0;
   
    int framesCounter = 0;
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen)
        {
            case GAMEPLAY:
            {
                // Update GAMEPLAY screen data here!
               framesCounter++;
       
               if (framesCounter >= 6)
               {
                   if (sourceRec.x > 96)//spriteWidth*3 
                   {
                       sourceRec.x = 0;
                   }
                   framesCounter = 0;
                   sourceRec.x += spriteWidth;          
 
               }
 
            } break;
            case END:
            {
                // Update END screen data here!
                if (IsKeyPressed(KEY_ENTER))
                {
                    screen = EXIT;
                    timeCounter = 10;
                }
 
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
       
            ClearBackground(RAYWHITE);
           
            switch(screen)
            {
                case GAMEPLAY:
                {
                    // Draw GAMEPLAY screen here!
 
                 DrawText("Congrats! You created your first window!", 190, 200, 20, LIGHTGRAY);
           
                 DrawTexturePro(characters, sourceRec, destinationRec, origin, 0, WHITE);
           
                 DrawTexture(characters, 0, 0, WHITE);  
                 
                } break;
                case END:
                {
                    // Draw END screen here!
                    DrawText("YOU LOOOSE... this is the end...", 100, 200, 30, DARKGREEN);
 
                } break;
                default: break;
            }
       
            DrawFPS(10, 10);
       
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
   
    return 0;
}