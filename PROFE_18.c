#include "raylib.h"
 
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1280;
    const int screenHeight = 720;
    const int velocidady = 8;
    const int ballSize = 25;
   
   
    Vector2 paladerecha, palaizquierda;
    Vector2 tamanorectangle;
 
    tamanorectangle.x = 20;
    tamanorectangle.y = 100;
   
    paladerecha.x = screenWidth - 50 - tamanorectangle.x;
    paladerecha.y = screenHeight/2 - tamanorectangle.y/2;
   
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - tamanorectangle.y/2;
   
   
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    Vector2 ballVelocity;
    ballVelocity.x = 8;
    ballVelocity.y = 8;
 
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
 
    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // Gestiono pulsaciones botones
        if (IsKeyDown(KEY_Q)){
          palaizquierda.y -= velocidady;
        }
       
        if (IsKeyDown(KEY_A)){
          palaizquierda.y += velocidady;
        }
       
        if (IsKeyDown(KEY_UP)){
          paladerecha.y -= velocidady;
        }
       
        if (IsKeyDown(KEY_DOWN)){
          paladerecha.y += velocidady;
        }
       
        //Consulto Limites
        if(palaizquierda.y<0){
            palaizquierda.y = 0;
        }
       
        if(palaizquierda.y > (screenHeight - tamanorectangle.y)){
            palaizquierda.y = screenHeight - tamanorectangle.y;
        }
       
        if(paladerecha.y<0){
            paladerecha.y = 0;
        }
       
        if(paladerecha.y > (screenHeight - tamanorectangle.y)){
            paladerecha.y = screenHeight - tamanorectangle.y;
        }
       
        //Gestionamos el movimiento de la Bola
        ball.x += ballVelocity.x;
        ball.y += ballVelocity.y;
       
        if((ball.x > screenWidth - ballSize) || (ball.x < ballSize) ){
            ballVelocity.x *=-1;
        }
       
        if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
            ballVelocity.y *=-1;
        }
 
       
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(BLUE);
 
            // Pinto Pala Derecha
            DrawRectangleV(paladerecha, tamanorectangle, GREEN);
           
            // Pinto Pala Izquierda
            DrawRectangleV(palaizquierda, tamanorectangle, GREEN);
           
            // Pintamos la pelota
            DrawCircleV(ball, ballSize, GREEN);  
           
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}