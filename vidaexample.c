#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
 
    InitWindow(screenWidth, screenHeight, "Template");
    int frameCount = 0;
    int frameCount2 = 0;
    int timeCounter = 0;
   
   //examen 
    //Rectangle backRect = { screenWidth/2 - 200, screenHeight/2 - 50, 400, 100}; //Background Rectangle 
    Rectangle backRect;
    backRect.width = 100;
    backRect.height = 20;    
    backRect.x = 50;
    backRect.y = screenHeight/10 - backRect.height/2;
    int margin = 5;
    Rectangle fillRect = { backRect.x + margin, backRect.y + margin, backRect.width - (2 * margin), backRect.height - (2 * margin)};
    Rectangle lifeRect = fillRect;
    //lifeRect.width /= 2;//comensar en el 0
    Color lifeColor = YELLOW;
   //EXAMEN
    //bool blink = true;
    bool win = false;
    bool lose = false;
   //EXAM
    int winNum = 0;
    int loseNum = 0;
   
    int drainLife = 1;
    int healLife = 15;
   //EXAM
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here        
               
        //----------------------------------------------------------------------------------
       
       
        if (!win && !lose)
        {
            if (IsKeyPressed(KEY_SPACE)) lifeRect.width -= healLife;
            //else lifeRect.width -= drainLife;
       
            else if (lifeRect.width <= 0)// eliminar la victoria
            {
                lose = true;
                ++loseNum;
                lifeRect.width = fillRect.width;
            }
         
           //exAMEN
            if (lifeRect.width >= 2*(fillRect.width/3))
            {
                lifeColor = GREEN;
            }
           
            else if (lifeRect.width <= fillRect.width/3)
            {
                lifeColor = ORANGE;
            }
           
            else
            {
                lifeColor = YELLOW;
            }
            //EXAMEN
        }
             
        else
        {
            if (IsKeyPressed(KEY_R))
            {
                win = false;
                lose = false;
                lifeRect.width = fillRect.width;
            }
        }
       
       
       
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
            //DrawText("MY GAME", (screenWidth - MeasureText("MY GAME", 60))/2, screenHeight/2 - (screenHeight/4), 60, PURPLE);
            //if (blink) DrawText("press start", (screenWidth - MeasureText("press start", 20))/2, screenHeight/2 + (screenHeight/5), 20, BLACK);
            DrawRectangleRec (backRect, BLACK);
            DrawRectangleRec (fillRect, RED);
            DrawRectangleRec (lifeRect, lifeColor);
            //if (win == false && lose == false) DrawText("Press Space!", (screenWidth - MeasureText("Press Space!", 60))/2, screenHeight/2 - (screenHeight/4), 60, BLACK);
            //if (blink) DrawText("FIGHT!", (screenWidth - MeasureText("FIGHT!", 60))/2, screenHeight/2 + (screenHeight/5), 60, BLACK);
            //if (win) DrawText("YOU WIN!", (screenWidth - MeasureText("YOU WIN!", 60))/2, screenHeight/2 - (screenHeight/4), 60, GREEN);
            if (lose) DrawText("YOU LOSE", (screenWidth - MeasureText("YOU LOSE", 60))/2, screenHeight/2 - (screenHeight/4), 60, RED);
            if (win || lose) DrawText("PRESS R TO RETRY", (screenWidth - MeasureText("PRESS R TO RETRY", 40))/2, screenHeight/2 + (screenHeight/5), 40, PURPLE);
            if (win || lose) DrawText("PRESS F TO FINAL THIS GAME", (screenWidth - MeasureText("PRESS F TO FINAL THIS GAME", 40))/2, screenHeight/2 + (screenHeight/3), 40, PURPLE);
            //if (lifeRect.width <= fillRect.width/3) DrawText("NEVER GIVE UP!", (screenWidth - MeasureText("NEVER GIVE UP!", 40))/2, screenHeight/2 + (screenHeight/3), 40, ORANGE);
            //if (lifeRect.width >= 2*(fillRect.width/3)) DrawText("YOU'RE ALMOST THERE!", (screenWidth - MeasureText("YOU'RE ALMOST THERE!", 40))/2, screenHeight/2 + (screenHeight/3), 40, GREEN);
           
            DrawText(FormatText("Wins: %i", winNum), screenWidth/4 - MeasureText(FormatText("Wins: %i", winNum), 40)/2, screenHeight/7, 40, BLACK);
            DrawText(FormatText("Loses: %i", loseNum), 3*screenWidth/4 - MeasureText(FormatText("Loses: %i", loseNum), 40)/2, screenHeight/7, 40, BLACK);
           
           
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------  
   
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    return 0;
}