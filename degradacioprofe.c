
/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
 
#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
 
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
   
    Color recColor = RED;
 
    Vector2 point1,point2,point3;
    point1.x = screenWidth/2;
    point1.y = screenHeight/2;
   
    point2.x = screenWidth/2 - 60;
    point2.y = screenHeight/2 +50;
   
    point3.x = screenWidth/2 + 60;
    point3.y = screenHeight/2 +50;
   
   
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        if(fadeOut)
        {
            alpha += fadeSpeed;
           
            if(alpha >= 1.0f)
            {
                alpha = 1.0f;
                fadeOut = !fadeOut;
            }
        }
        else
        {
            alpha -= fadeSpeed;
            if(alpha <= 0.0f)
            {
                alpha = 0.0f;
                fadeOut = !fadeOut;
            }
        }
       
        if(IsKeyPressed(KEY_SPACE))
        {
            fadeOut = !fadeOut;
        }
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
                       
            DrawTriangle(point1, point2, point3, Fade(RED, alpha));
           
            DrawText(FormatText("%f", alpha), 10,10, 20, WHITE);
 
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------  
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}